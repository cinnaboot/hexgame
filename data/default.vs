#version 330 core

layout (location = 0) in vec3 vertexPosition_modelspace;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 texCoord;

out vec3 fragVertex;
out vec3 fragNormal;
out vec2 fragUV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	fragNormal = normal;
	fragVertex = vertexPosition_modelspace;
	fragUV = texCoord.st;
	gl_Position = projection * view * model * vec4(vertexPosition_modelspace, 1);
}

