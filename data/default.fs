#version 330 core

//in vec3 fragColor; // NOTE: removing this line fails to link program... somehow?
in vec3 fragVertex;
in vec3 fragNormal;
in vec2 fragUV;

out vec4 color;

uniform mat4 model;
uniform mat3 normal_matrix;

struct point_light {
	uint light_ID;
	vec3 position;
	vec3 color;
	float intensity;
};
#define MAX_LIGHTS 10
uniform point_light lights[MAX_LIGHTS];
uniform uint num_lights = 0u;

uniform sampler2D sampler;


void main()
{
	vec3 normal = normalize(normal_matrix * fragNormal);
	vec3 fragPosition = vec3(model * vec4(fragVertex, 1));
	float totalBrightness = 0;

	for (uint i = 0u; i < num_lights; i++) {
		vec3 surfaceToLight = lights[i].position - fragPosition;
		float brightness = dot(normal, surfaceToLight) / (length(surfaceToLight) * length(normal));
		totalBrightness += brightness;
	}

	color = clamp(totalBrightness, 0, 1) * texture(sampler, fragUV.st);
}

