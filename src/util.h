
#pragma once

#include <cstdint>
#include <cassert>


typedef float real32;
typedef float GLfloat;
typedef double real64;
typedef int32_t bool32;
typedef int32_t int32;
typedef int64_t int64;
typedef uint8_t uint8;
typedef uint32_t uint32;
typedef uint32_t uint;


struct v2f
{
	v2f(): x(0), y(0) {}
	v2f(real64 a, real64 b): x(a), y(b) {}
	real64 x;
	real64 y;
};

struct v2i
{
	v2i(int a, int b): x(a), y(b) {}
	v2i() : x(0), y(0) {}
	int32 x;
	int32 y;
};

struct v3f
{
	v3f(): x(0), y(0), z(0) {}
	v3f(real64 a, real64 b, real64 c): x(a), y(b), z(c) {}
	real64 x;
	real64 y;
	real64 z;
};

struct v3i
{
	int32 x;
	int32 y;
	int32 z;
};

struct v4i
{
	int32 x0;
	int32 y0;
	int32 x1;
	int32 y1;
};

struct util_RGBA
{
	real32 R;
	real32 G;
	real32 B;
	real32 A;
};

struct util_image
{
	int32 w;
	int32 h;
	int32 bits_per_channel;
	int32 num_channels;
	uint data_len;
	uint8* pixels;
	char file_path[256];
};

inline real32
SafeRatio(real32 dividend, real32 divisor)
{
	if (divisor == 0)
		// TODO: log warning (don't require aixlog)
		return 0;
	else
		return dividend / divisor;
}

inline int32
SafeTruncateToInt32(int64 val)
{
	assert(val <= INT32_MAX && val >= INT32_MIN);
	return (int32) val;
}

inline void
utilConvertColor(GLfloat buf[3], uint32 color)
{
	// NOTE: not using the alpha values
	buf[0] = (GLfloat) ((color >> 24) & 0xFF) / (GLfloat) 255;
	buf[1] = (GLfloat) ((color >> 16) & 0xFF) / (GLfloat) 255;
	buf[2] = (GLfloat) ((color >> 8) & 0xFF) / (GLfloat) 255;
}

//-----------------
// C Strings

// NOTE: max_len should be the allocated size of dest
bool utilCopyCStr(char* dest, const char* src, uint max_len);

// NOTE: returns new string with '/' between
// NOTE: max_len should be the size of return buffer.
bool utilConcatPath(char* out, const char* base_dir, const char* file_name, uint max_len);

const char* utilBaseName(const char* path_str);

// NOTE: returns true if the first 'sz' characters from each string match
bool utilMatchPrefix(const char* lhs, const char* rhs, int sz);

//-----------------
// Memory allocation

// NOTE: Wrapper for calloc that will send error message on out of memory
#define UTIL_ALLOC(len, type) (type *) utilLogAlloc((len), sizeof(type), __FILE__, __LINE__)
void* utilLogAlloc(uint item_count, uint type_size, const char* file_name, const int line);

void utilSafeFree(const void* mem);

//-----------------
// File I/O

char* utilDumpTextFile(const char* filename);

bool utilWriteTextFile(const char* filename, const char* text);

//-----------------
// utilImage

util_image utilLoadImage(const char* base_dir, const char* filename);

void utilFreeImage(util_image image);

v2f utilGetPaletteCoords(util_image& palette_image, uint color_index);
