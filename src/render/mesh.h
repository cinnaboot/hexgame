/*
 * mesh.h
 * 	- wrapper for assimp http://www.assimp.org
 */

#pragma once

#include <glm/glm.hpp>

#include "../util.h"

struct meMeshInfo
{
	glm::mat4 model_transform;
	uint num_vertices;
	glm::vec3* vertices;
	glm::vec3* normals;
	glm::vec3* texture_coords; // NOTE: using vec3 to stay aligned with other props
	uint num_indices;
	uint* indices;
	glm::vec3 diffuse_color;

	bool use_texture;
	util_image diffuse_texture;
};

struct meMeshGroup
{
	bool use_normals;
	uint num_meshes;
	meMeshInfo** meshes;
	// animation/bonemapping info here...
};


bool meInitAssimp();

bool meLoadFromFile(meMeshGroup& mesh_group, const char* data_dir, const char* filename);

void meFreeMeshGroup(meMeshGroup& mesh_group);

void meShutdownAssimp();

