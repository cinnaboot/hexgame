
#pragma once

#include <GL/gl3w.h>
#include <glm/glm.hpp>

#include "../util.h"


// TODO: can these structs be used as opaque pointers?
struct gl_buffer
{
	GLuint buffer_id;
	uint count; // NOTE: number of elements in buffer
	uint max_count; // NOTE: maxium number of elements buffer can fit
	GLfloat* buffer;
};

struct gl_index_buffer
{
	GLuint buffer_id;
	uint count; // NOTE: number of elements in buffer
	uint max_count; // NOTE: maxium number of elements buffer can fit
	uint* buffer;
};

struct render_object
{
	// TODO: can remove these once we're using the global color palatte for all textures
	bool use_texture;
	GLuint tex_id;

	gl_buffer vertex_buffer;
	gl_buffer normal_buffer;
	gl_buffer uv_buffer;
	gl_index_buffer index_buffer;
};

struct rg_shader_program
{
	GLuint program_id;

	GLuint model_matrix_id;
	GLuint view_matrix_id;
	GLuint projection_matrix_id;
	GLuint normal_matrix_id;

	GLuint vertex_array_id;
	GLuint sampler_id;
	GLuint num_lights_id;
};

struct render_group
{
	uint num_objects;
	render_object** render_objects;
	rg_shader_program shader;
	bool use_normals;
	bool draw_indexed;
	GLenum draw_mode = GL_TRIANGLES;
};

// TODO: maybe pull out to seperate header
struct rg_point_light
{
	uint light_ID;
	glm::vec3 position;
	glm::vec3 color;
	float intensity;
};

render_object * rgAllocateRenderObject(uint buffer_len, uint index_len = 0);

// NOTE: initializes a render_group with 1 render_object allocated
render_group* rgInitSingle(rg_shader_program& sp, uint vertex_buffer_len,
						bool use_normals = false, uint index_buffer_len = 0,
						GLenum draw_mode = GL_TRIANGLES);

bool rgInitShaderProgram(rg_shader_program& sp, const char * vertex_code, const char * frag_code);

bool rgInitGLTexture(GLuint& tex_id, util_image image);

void rgInitGLFloatBuffer(gl_buffer* buffer, GLenum usage, GLenum target);

void rgInitGLIndexBuffer(gl_index_buffer* index_buffer, GLenum usage, GLenum target);

void rgUpdateGLBuffer(gl_buffer& buffer);

void rgDraw(render_group* rg, glm::mat4 model_matrix,
		glm::mat4 view_matrix, glm::mat4 projection_matrix,
		rg_point_light* lights, uint num_lights,
		bool update_vertex_data = false);

void rgFree(render_group* rg);

