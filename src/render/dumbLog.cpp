
#include <ctime>
#include <chrono>

#include "dumbLog.h"

void dumbLog::setOutputStream(std::ostream* out)
{
	this->OUT = out;
}

const char*
dumbLog::logLevelToString(log_level level)
{
	switch (level) {
		case log_level::Error: return "Error";
		case log_level::Warning: return "Warning";
		case log_level::Info: return "Info";
		default: return "Potato";
	}
};

std::tm*
dumbLog::getCurrentTime()
{
	auto now = std::chrono::system_clock::now();
	auto t_c = std::chrono::system_clock::to_time_t(now);
	return std::localtime(&t_c);
}

int
dumbLog::getCurrentMS()
{
	auto now = std::chrono::system_clock::now();
	long long total_ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
	return int(total_ms % 1000);
}

