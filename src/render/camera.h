
#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../util.h"


struct camera
{
	float hAngle;
	float vAngle;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 up;
	glm::vec3 left;
	glm::vec3 target;
	glm::vec3 world_up;

	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat4 MVP;
};

enum projection_type
{
	PERSPECTIVE,
	ORTHOGRAPHIC,
};


v2f cameraUnproject(camera& cam, int x, int y, int vp_width, int vp_height);

v3f cameraCreateRay(camera& cam, v2i vp_coords, v2i vp_dims);

bool cameraIntersectPlane(camera& cam, v3f ray, v3f plane_origin, v3f plane_normal, v3f& intersection);

void cameraInitPerspective(camera& cam, glm::vec3 position, glm::vec3 target, glm::vec3 world_up);

void cameraMove(camera& cam, bool up, bool left, bool down, bool right, bool forward, bool backward);

void cameraRotate(camera& cam, int32	xrel, int32 yrel);

void cameraRoll(camera& cam, bool CW, bool CCW);
