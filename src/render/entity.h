
#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../hexgrid.h"
#include "mesh.h"
#include "render_group.h"

struct ent_grid_position
{
	int q;
	int r;
	int s;
};

struct Entity
{
	glm::mat4 world_transform;
	glm::vec3 scale;
	glm::vec3 translation;
	glm::vec4 rotation;

	meMeshGroup mesh_group;
	render_group* ren_group;

	char model_filename[256];

	bool use_grid; // NOTE: determines if entity is position with grid_pos or translation
	ent_grid_position grid_pos;
	hex_info* container_hex;
};

inline void
entTranslate(Entity& e, glm::vec3 v)
{
	e.world_transform = glm::translate(e.world_transform, v);
}

inline void
entScale(Entity& e, glm::vec3 v)
{
	e.world_transform = glm::scale(e.world_transform, v);
}

bool entInit(Entity& e, const char* data_dir, rg_shader_program& shader, hexgrid& hg);

void entFree(Entity& e);

bool entPositionToGrid(Entity& e, hexgrid& hg, int q, int r, int s);

void entSetWorldPosition(Entity& e, float x, float y, float z);
