
#pragma once

#include <vector>

#if defined (_WIN32)
	#include <SDL.h>
#else
	#include <SDL2/SDL.h>
#endif
#include <GL/gl3w.h>
#include <glm/glm.hpp>

#include "camera.h"
#include "entity.h"
#include "../hexgrid.h"
#include "render_group.h"
#include "../util.h"


struct SDL_Handles
{
	SDL_Window *window;
	SDL_GLContext glContext;
	SDL_DisplayMode currentDisplayMode;
	std::vector<SDL_Surface*> texSurfaces;
};

struct rg_point_light;

struct render_state
{
	v2i viewport_dims;
	bool is_debug_draw;
	SDL_Handles handles;
	camera cam;
	util_image palette_image;
	// TODO: this needs to be initialized outside struct because calloc
	GLuint palette_id = UINT_MAX;

	render_group* filled_hex_render_group;
	render_group* hex_line_render_group;
	render_group* debug_render_group;
	// NOTE: entity render groups are stored on the entity object
	rg_shader_program default_shader;

	rg_point_light* lights;
	uint num_lights;
	uint max_lights;
};

bool renInit(render_state* rs);

void renFreeBuffers(render_state* rs);

void renRenderFrame(render_state* rs, hexgrid& hg, Entity* entities, uint32 entity_count);

void renRenderDebug(render_state* rs, std::vector<Point> &vertices);

