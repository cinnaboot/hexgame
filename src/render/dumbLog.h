
#include <iomanip>
#include <iostream>


enum log_level {
	Error,
	Warning,
	Info,
	Debug
};

struct dumbLog
{
	std::ostream* OUT = &std::cout;
	void setOutputStream(std::ostream* out);
	const char* logLevelToString(log_level level);
	std::tm* getCurrentTime();
	int getCurrentMS();
};

static dumbLog logger;

#define LOG(level) *logger.OUT \
	<< std::put_time(logger.getCurrentTime(), "%F %T.") << logger.getCurrentMS() << " " \
	<< "[" << logger.logLevelToString(level) << "] " \
	<< "(" << __FUNCTION__ << ") "

