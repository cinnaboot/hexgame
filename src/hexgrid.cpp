
#include "aixlog.hpp"

#include "hexgrid.h"

#define MAX_HEX_COUNT 8192 // NOTE: padding some extra space for hex buffers to grow
#define IDX_PER_HEX_FILLED 54 // NOTE: 6 triangles * 3 vertices per triangle * 3 floats per vertex
#define IDX_PER_HEX_LINES 36 // NOTE: 6 lines * 2 vertices per line * 3 floats per vertex


// forward declarations
hex_info createHexInfo(hexgrid& hg, int q, int r);
void createHexagonGrid(hexgrid& hg);
void populateFilledHexGLBuffers(hexgrid& hg, hex_info& hxi, render_group* rg_filled);
void populateLineGLBuffers(hexgrid& hg, hex_info& hxi, render_group* rg_lines);


// interface

bool
hgInit(
	hexgrid& hg,
	render_group*& rg_filled,
	render_group*& rg_lines,
	rg_shader_program& shader)
{
	Orientation o = (hg.layout_mode == LAYOUT_FLAT) ? layout_flat : layout_pointy;
	hg.hexlib_layout = Layout(o, Point(hg.hex_size, hg.hex_size), Point(hg.position.x, hg.position.y));

	switch (hg.gridT) {
		case HEXAGON:
			createHexagonGrid(hg);
			break;
		case HASH_MAP: // NOTE: for HASH_MAP the file will be parsed in scene_loader
			break;
		default:
			LOG(ERROR) << "Unhandled grid type\n";
			return false;
	}

	uint hex_count = hg.hex_map.size();
	assert(hex_count < MAX_HEX_COUNT);

	// init render_groups
	uint buf_len = MAX_HEX_COUNT * IDX_PER_HEX_FILLED;
	rg_filled = rgInitSingle(shader, buf_len, true);
	buf_len = MAX_HEX_COUNT * IDX_PER_HEX_LINES;
	rg_lines = rgInitSingle(shader, buf_len, true, 0, GL_LINES);

	if ((rg_filled == nullptr) || (rg_lines == nullptr)) {
		LOG(ERROR) << "Error allocating render_group, exiting\n";
		return false;
	}

	render_object* ro_f = rg_filled->render_objects[0];
	render_object* ro_l = rg_lines->render_objects[0];
	ro_f->vertex_buffer.count = hex_count * IDX_PER_HEX_FILLED;
	ro_f->normal_buffer.count = hex_count * IDX_PER_HEX_FILLED;
	ro_f->uv_buffer.count = hex_count * IDX_PER_HEX_FILLED;
	ro_l->vertex_buffer.count = hex_count * IDX_PER_HEX_LINES;
	ro_l->normal_buffer.count = hex_count * IDX_PER_HEX_LINES;
	ro_l->uv_buffer.count = hex_count * IDX_PER_HEX_LINES;
	ro_f->use_texture = true;
	ro_l->use_texture = true;

	// populate buffers
	for (auto it : hg.hex_map) {
		populateFilledHexGLBuffers(hg, it.second, rg_filled);
		populateLineGLBuffers(hg, it.second, rg_lines);
	}

	// send buffers to GL
	rgInitGLFloatBuffer(&ro_f->vertex_buffer, GL_DYNAMIC_DRAW, GL_ARRAY_BUFFER);
	rgInitGLFloatBuffer(&ro_f->normal_buffer, GL_STATIC_DRAW, GL_ARRAY_BUFFER);
	rgInitGLFloatBuffer(&ro_f->uv_buffer, GL_DYNAMIC_DRAW, GL_ARRAY_BUFFER);
	rgInitGLFloatBuffer(&ro_l->vertex_buffer, GL_DYNAMIC_DRAW, GL_ARRAY_BUFFER);
	rgInitGLFloatBuffer(&ro_l->normal_buffer, GL_STATIC_DRAW, GL_ARRAY_BUFFER);
	rgInitGLFloatBuffer(&ro_l->uv_buffer, GL_DYNAMIC_DRAW, GL_ARRAY_BUFFER);

	return true;
}

bool
hgAddHex(hexgrid& hg, v3f hex_coords, render_group* rg_filled, render_group* rg_lines)
{
	hex_info* hxi = hgGetSingleHex(hg, hex_coords.x, hex_coords.y);
	if (!hxi) {
		// sanity checks
		render_object* ro_f = rg_filled->render_objects[0];
		gl_buffer& vb_f = ro_f->vertex_buffer;
		gl_buffer& nb_f = ro_f->normal_buffer;
		gl_buffer& ub_f = ro_f->uv_buffer;
		render_object* ro_l = rg_lines->render_objects[0];
		gl_buffer& vb_l = ro_l->vertex_buffer;
		gl_buffer& nb_l = ro_l->normal_buffer;
		gl_buffer& ub_l = ro_l->uv_buffer;

		if (vb_f.count + IDX_PER_HEX_FILLED < vb_f.max_count &&
			nb_f.count + IDX_PER_HEX_FILLED < nb_f.max_count &&
			ub_f.count + IDX_PER_HEX_FILLED < ub_f.max_count &&
			vb_l.count + IDX_PER_HEX_LINES < vb_l.max_count &&
			nb_l.count + IDX_PER_HEX_LINES < nb_l.max_count &&
			ub_l.count + IDX_PER_HEX_LINES < ub_l.max_count)
		{
			vb_f.count += IDX_PER_HEX_FILLED;
			nb_f.count += IDX_PER_HEX_FILLED;
			ub_f.count += IDX_PER_HEX_FILLED;
			vb_l.count += IDX_PER_HEX_LINES;
			nb_l.count += IDX_PER_HEX_LINES;
			ub_l.count += IDX_PER_HEX_LINES;

			Hex h = hex_round(pixel_to_hex(hg.hexlib_layout, Point(hex_coords.x, hex_coords.y)));
			hex_info new_hex = createHexInfo(hg, h.q, h.r);
			hg.hex_map.insert({h, new_hex});
			populateFilledHexGLBuffers(hg, new_hex, rg_filled);
			populateLineGLBuffers(hg, new_hex, rg_lines);

			// NOTE: we could send just a part of the buffer if performance ends up being bad here
			rgUpdateGLBuffer(vb_f);
			rgUpdateGLBuffer(nb_f);
			rgUpdateGLBuffer(ub_f);
			rgUpdateGLBuffer(vb_l);
			rgUpdateGLBuffer(nb_l);
			rgUpdateGLBuffer(ub_l);

			return true;
		} else {
			LOG(ERROR) << "that hex isn't going to fit in the buffer(s)\n";
			return false;
		}
	}

	return false;
}

bool
hgRemoveHex(hexgrid& hg, v3f hex_coords, render_group* rg_filled, render_group* rg_lines)
{
	hex_info* hxi = hgGetSingleHex(hg, hex_coords.x, hex_coords.y);
	if (hxi) {
		render_object* ro_f = rg_filled->render_objects[0];
		gl_buffer& vb_f = ro_f->vertex_buffer;
		gl_buffer& nb_f = ro_f->normal_buffer;
		gl_buffer& ub_f = ro_f->uv_buffer;
		render_object* ro_l = rg_lines->render_objects[0];
		gl_buffer& vb_l = ro_l->vertex_buffer;
		gl_buffer& nb_l = ro_l->normal_buffer;
		gl_buffer& ub_l = ro_l->uv_buffer;

		// NOTE: don't need sanity checks to delete from buffers
		vb_f.count -= IDX_PER_HEX_FILLED;
		nb_f.count -= IDX_PER_HEX_FILLED;
		ub_f.count -= IDX_PER_HEX_FILLED;
		vb_l.count -= IDX_PER_HEX_LINES;
		nb_l.count -= IDX_PER_HEX_LINES;
		ub_l.count -= IDX_PER_HEX_LINES;

		size_t erased = hg.hex_map.erase(hxi->hex);
		assert(erased == 1);

		// NOTE: have to repopulate all the buffers, and re-index
		uint i = 0;
		for (auto& it : hg.hex_map) {
			it.second.hexID = i;
			populateFilledHexGLBuffers(hg, it.second, rg_filled);
			populateLineGLBuffers(hg, it.second, rg_lines);
			i++;
		}

		// NOTE: we do need to update the whole buffer when removing data
		rgUpdateGLBuffer(vb_f);
		rgUpdateGLBuffer(nb_f);
		rgUpdateGLBuffer(ub_f);
		rgUpdateGLBuffer(vb_l);
		rgUpdateGLBuffer(nb_l);
		rgUpdateGLBuffer(ub_l);

		return true;
	}

	return false;
}

void
hgUpdateUVBuffer(hexgrid& hg, render_group* rg)
{
	gl_buffer& uv_buf = rg->render_objects[0]->uv_buffer;
	assert(uv_buf.count == hg.hex_map.size() * IDX_PER_HEX_FILLED);

	for (auto& it : hg.hex_map) {
		hex_info hxi = it.second;
		v2f uv_coords = (hxi.selected) ? hg.selected_fill_color_uv : hg.fill_color_uv;
		uint buf_idx = hxi.hexID * IDX_PER_HEX_FILLED;

		for (uint j = 0; j < IDX_PER_HEX_FILLED; j +=3) {
			uv_buf.buffer[buf_idx + j + 0] = uv_coords.x;
			uv_buf.buffer[buf_idx + j + 1] = uv_coords.y;
			uv_buf.buffer[buf_idx + j + 2] = 0;
		}
	}

	rgUpdateGLBuffer(uv_buf);
}

hex_info*
hgGetSingleHex(hexgrid& hg, real32 x, real32 y)
{
	Point p(x, y);
	Hex h = hex_round(pixel_to_hex(hg.hexlib_layout, p));

	auto it = hg.hex_map.find(h);

	if (it != hg.hex_map.end())
		return &it->second;

	return nullptr;
}

hex_info*
hgGetSingleHex(hexgrid& hg, int q, int r, int s)
{
	auto it = hg.hex_map.find(Hex(q, r, s));

	if (it != hg.hex_map.end())
		return &it->second;

	return nullptr;
}

v3f
hgGetWorldPostion(hexgrid& hg, int q, int r, int s)
{
	Point p = hex_to_pixel(hg.hexlib_layout, Hex(q,r,s));
	return v3f(p.x, p.y, hg.position.z); // NOTE: this doesn't account for hg.normal
}

void
hgResetHexes(hexgrid& hg)
{
	hg.start_hex = hg.current_hex = nullptr;

	for (auto& it : hg.hex_map)
		it.second.selected = false;
}

void
hgUpdateHexFill(hexgrid& hg, int32 x, int32 y)
{
	hex_info *hxi = hgGetSingleHex(hg, x, y);
	if (hxi && (hxi != hg.current_hex) && hg.start_hex)
	{
		hg.current_hex = hxi;
		int l = hex_distance(hg.start_hex->hex, hg.current_hex->hex);

		for (auto& it: hg.hex_map) {
			if (hex_distance(hg.start_hex->hex, it.second.hex) <= l)
				it.second.selected = true;
			else
				it.second.selected = false;
		}
	}
}

void
hgUpdateHexLineDraw(hexgrid& hg, int32 x, int32 y)
{
	hex_info *hxi = hgGetSingleHex(hg, x, y);
	if (hxi && (hxi != hg.current_hex) && hg.start_hex)
	{
		// TODO: can avoid this loop by caching selected hexes
		for (auto& it : hg.hex_map)
			it.second.selected = false;

		hg.current_hex = hxi;
		vector<Hex> hexLine = hex_linedraw(hg.start_hex->hex, hxi->hex);

		for (Hex h : hexLine) {
			auto it = hg.hex_map.find(h);

			if (it != hg.hex_map.end())
				it->second.selected = true;
		}
	}
}

void
hgUpdateHexConeFill(hexgrid& hg, int32 x, int32 y, float cone_angle, std::vector<Point>& debug_vertices)
{
	hex_info *hxi = hgGetSingleHex(hg, x, y);
	if (hxi && (hxi != hg.current_hex) && hg.start_hex)
	{
		hg.current_hex = hxi;

		// TODO: Remove debug code here and from gooey.h
		Point p1(hg.start_hex->XPos, hg.start_hex->YPos);
		Point p2(hg.current_hex->XPos, hg.current_hex->YPos);
		real64 angle = std::atan2(p2.y - p1.y, p2.x - p1.x);
		real64 len = std::hypot(p2.y - p1.y, p2.x - p1.x);
		real64 coneAngle = cone_angle * M_PI / 180; // M_PI is non-standard and may not be portable

		real64 x1 = len * std::cos(angle);
		real64 y1 = len * std::sin(angle);
		// top of cone
		real64 topX = x1 * std::cos(coneAngle) - y1 * std::sin(coneAngle) + p1.x;
		real64 topY = x1 * std::sin(coneAngle) + y1 * std::cos(coneAngle) + p1.y;
		// bottom of cone
		real64 botX = x1 * std::cos(coneAngle) + y1 * std::sin(coneAngle) + p1.x;
		real64 botY = x1 * std::sin(-1 * coneAngle) + y1 * std::cos(coneAngle) + p1.y;

		Point test_p;
		Point vert2 = Point(botX, botY);
		Point vert4 = Point(topX, topY);
		std::vector<Point> vertices = {p1, vert2, p2, vert4};

		for (auto& it : hg.hex_map) {
			hex_info& h = it.second;

			test_p.x = h.XPos;
			test_p.y = h.YPos;

			if (crossingTest(vertices, test_p))
				h.selected = true;
			else
				h.selected = false;
		}

		debug_vertices[0] = p1;
		debug_vertices[1] = vert2;
		debug_vertices[2] = p2;
		debug_vertices[3] = vert4;
	}
}

// internal

hex_info
createHexInfo(hexgrid& hg, int q, int r)
{
	hex_info hxi;
	hxi.hexID = (int32) hg.hex_map.size();
	hxi.selected = false;
	hxi.hex.q = q; hxi.hex.r = r; hxi.hex.s = -q-r;
	Point p = hex_to_pixel(hg.hexlib_layout, hxi.hex);
	hxi.XPos = p.x;
	hxi.YPos = p.y;
	hxi.vertices = polygon_corners(hg.hexlib_layout, hxi.hex);
	hxi.vertices.shrink_to_fit();

	return hxi;
}

void
createHexagonGrid(hexgrid& hg)
{
	int hr = hg.hex_radius;
	int nhr = hg.hex_radius * -1;

	for (int q = nhr; q <= hr; q++) {
		int r1 = std::max(nhr, -q - hr);
		int r2 = std::min(hr, -q + hr);

		for (int r = r1; r <= r2; r++) {
			hex_info hxi = createHexInfo(hg, q, r);
			hg.hex_map.insert({hxi.hex, hxi});
		}
	}
}

void
populateFilledHexGLBuffers(hexgrid& hg, hex_info& hxi, render_group* rg_filled)
{
	render_object* ro = rg_filled->render_objects[0];
	gl_buffer& vbuf = ro->vertex_buffer;
	gl_buffer& normal_buf = ro->normal_buffer;
	gl_buffer& uv_buf = ro->uv_buffer;
	uint idx = hxi.hexID * IDX_PER_HEX_FILLED;
	v2f uv_coords = (hxi.selected) ? hg.selected_fill_color_uv : hg.fill_color_uv;

	for (uint i = 0; i < IDX_PER_HEX_FILLED; i += 3) {
		// cheat at vertex normals since all hexes lay flat on z-axis
		normal_buf.buffer[idx + i + 0] = 0.f;
		normal_buf.buffer[idx + i + 1] = 0.f;
		normal_buf.buffer[idx + i + 2] = 1.f;

		// fill uv_buffer with palette texture coords
		uv_buf.buffer[idx + i + 0] = uv_coords.x;
		uv_buf.buffer[idx + i + 1] = uv_coords.y;
		uv_buf.buffer[idx + i + 2] = 0;
	}

	for (uint i = idx, j = 0; j < 6; i += 9, j++) {
		// vertex 0
		vbuf.buffer[i + 0] = (GLfloat) hxi.XPos;
		vbuf.buffer[i + 1] = (GLfloat) hxi.YPos;
		vbuf.buffer[i + 2] = (GLfloat) 0.f;

		// vertex 1
		vbuf.buffer[i + 3] = (GLfloat) hxi.vertices[j].x;
		vbuf.buffer[i + 4] = (GLfloat) hxi.vertices[j].y;
		vbuf.buffer[i + 5] = (GLfloat) 0.f;

		if (j == 5) {	// re-use the first point for the last triangle
			// vertex 2
			vbuf.buffer[i + 6] = (GLfloat) hxi.vertices[0].x;
			vbuf.buffer[i + 7] = (GLfloat) hxi.vertices[0].y;
			vbuf.buffer[i + 8] = (GLfloat) 0.f;
		} else {
			// vertex 2
			vbuf.buffer[i + 6] = (GLfloat) hxi.vertices[j + 1].x;
			vbuf.buffer[i + 7] = (GLfloat) hxi.vertices[j + 1].y;
			vbuf.buffer[i + 8] = (GLfloat) 0.f;
		}
	}
}

void
populateLineGLBuffers(hexgrid& hg, hex_info& hxi, render_group* rg_lines)
{
	render_object* ro = rg_lines->render_objects[0];
	gl_buffer& vbuf = ro->vertex_buffer;
	gl_buffer& normal_buf = ro->normal_buffer;
	gl_buffer& uv_buf = ro->uv_buffer;
	uint idx = hxi.hexID * IDX_PER_HEX_LINES;

	for (uint i = 0; i < IDX_PER_HEX_LINES; i += 3) {
		// cheat at vertex normals since all hexes lay flat on z-axis
		normal_buf.buffer[idx + i + 0] = 0.f;
		normal_buf.buffer[idx + i + 1] = 0.f;
		normal_buf.buffer[idx + i + 2] = 1.f;

		// fill uv_buffer with palette texture coords
		uv_buf.buffer[idx + i + 0] = hg.line_color_uv.x;
		uv_buf.buffer[idx + i + 1] = hg.line_color_uv.y;
		uv_buf.buffer[idx + i + 2] = 0;
	}

	Point p1, p2;
	for (int j = 0; j < 6; j ++) {
		if (j == 5) { // wrap
			p1 = hxi.vertices[j];
			p2 = hxi.vertices[0];
		} else {
			p1 = hxi.vertices[j];
			p2 = hxi.vertices[j + 1];
		}

		vbuf.buffer[idx + 0] = p1.x;
		vbuf.buffer[idx + 1] = p1.y;
		vbuf.buffer[idx + 2] = 0.f;
		vbuf.buffer[idx + 3] = p2.x;
		vbuf.buffer[idx + 4] = p2.y;
		vbuf.buffer[idx + 5] = 0.f;
		idx += 6;
	}
}
