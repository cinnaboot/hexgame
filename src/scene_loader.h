
#pragma once

#include "render/camera.h"
#include "hexgrid.h"
#include "render/render_group.h"
#include "util.h"


struct slSceneDoc;

slSceneDoc* slLoadFile(const char* data_dir, const char* scene_file, const char* schema_file);

void slFreeSceneDoc(slSceneDoc* sd);

bool slParseEntities(slSceneDoc* sd, Entity* entity_array, uint& entity_count, uint max_entities);

void slParseCamera(slSceneDoc* sd, camera& cam);

void slParseHexGrid(slSceneDoc* sd, hexgrid& hg, util_image& palette_image);

bool slParseGridHashMap(hexgrid& hg, const char* data_dir, const char* hexmap_schema_file);

bool slParseLights(slSceneDoc* sd, rg_point_light* lights, uint& num_lights, uint max_lights);

bool slSaveGridFile(hexgrid& hg);
