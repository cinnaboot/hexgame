
#pragma once

#include <glm/glm.hpp>

#include "render/entity.h"
#include "hexgrid.h"
#include "util.h"

enum game_select_mode
{
	HEX_SELECT,
	ENTITY_SELECT
};

// TODO: move this state into camera struct?
struct game_camera_movement_state
{
	bool is_camera_rotate;
	bool is_moveforward;
	bool is_movebackward;
	bool is_moveup;
	bool is_movedown;
	bool is_moveleft;
	bool is_moveright;
	bool is_rotateCW;
	bool is_rotateCCW;
};

struct game_state
{
	hexgrid grid;
	uint32 entity_count;
	Entity* entities;
	game_camera_movement_state movement;

	// testing entity selection
	game_select_mode select_mode;
	Entity* selected_entity;
};

