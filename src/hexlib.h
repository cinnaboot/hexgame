
#pragma once

// Generated code -- http://www.redblobgames.com/grids/hexagons/

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
using std::vector;


struct FractionalHex
{
    const double q;
    const double r;
    const double s;
    FractionalHex(double q_, double r_, double s_): q(q_), r(r_), s(s_) {}
};

struct Point
{
    double x;
    double y;
    Point(double x_, double y_): x(x_), y(y_) {}
	Point(): x(0), y(0) {}
};

struct Hex
{
    int q;
    int r;
    int s;
    Hex(int q_, int r_, int s_): q(q_), r(r_), s(s_) {}
	Hex(): q(0), r(0), s(0) {}

	bool operator==(const Hex& h) const {
		return (h.q == q && h.r == r && h.s == s);
	}
};

struct Orientation
{
    double f0;
    double f1;
    double f2;
    double f3;
    double b0;
    double b1;
    double b2;
    double b3;
    double start_angle;
    Orientation(double f0_, double f1_, double f2_, double f3_, double b0_,
			double b1_, double b2_, double b3_, double start_angle_):
		f0(f0_), f1(f1_), f2(f2_), f3(f3_), b0(b0_), b1(b1_), b2(b2_),
		b3(b3_), start_angle(start_angle_) {}
};

const Orientation layout_pointy = Orientation(sqrt(3.0), sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5);
const Orientation layout_flat = Orientation(3.0 / 2.0, 0.0, sqrt(3.0) / 2.0, sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, sqrt(3.0) / 3.0, 0.0);

struct Layout
{
    Orientation orientation;
    Point size;
    Point origin;
    Layout(Orientation orientation_, Point size_, Point origin_):
		orientation(orientation_), size(size_), origin(origin_) {}
};

int hex_length(Hex hex);
int hex_distance(Hex a, Hex b);
Hex hex_round(FractionalHex h);
vector<Hex> hex_linedraw(Hex a, Hex b);

Point hex_to_pixel(Layout layout, Hex h);
FractionalHex pixel_to_hex(Layout layout, Point p);
vector<Point> polygon_corners(Layout layout, Hex h);

// custom hex functions

bool hex_equal(Hex a, Hex b);

// NOTE: implementation of this line crossing test
// https://graphics.stanford.edu/pub/Graphics/RTNews/html/rtnv5n3.html#art3
// NOTE: assumes use of convex polygons with vertices in CCW layout
bool crossingTest(vector<Point> vertices, Point p);

