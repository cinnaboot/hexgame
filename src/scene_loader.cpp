
#include <string>

#include <glm/glm.hpp>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/error/en.h>
#include <rapidjson/writer.h>
#include "aixlog.hpp"

#include "render/entity.h"
#include "hexlib.h"
#include "render/mesh.h"
#include "scene_loader.h"


struct slSceneDoc
{
	rapidjson::Document* doc;
};

// TODO: put in global application config structure with other defines
static const char* g_data_dir;


// forward declarations
bool loadTextFile(char*& output, const char* data_dir, const char* file_name);
bool parseFile(rapidjson::Document* doc, const char* file_contents, const char* file_name);
glm::vec3 parseVec3(const rapidjson::Value& node);
glm::vec4 parseVec4(const rapidjson::Value& node);
bool validateJSON(rapidjson::Document* schema_doc, rapidjson::Document* doc,
		const char* json_file);
void addV3f(rapidjson::Writer<rapidjson::StringBuffer>& writer, float x, float y, float z);


// interface

slSceneDoc*
slLoadFile(const char* data_dir, const char* scene_file, const char* schema_file)
{
	LOG(INFO) << "Loading scene file: " << scene_file << "\n";
	rapidjson::Document schema_doc;
	slSceneDoc* sd = UTIL_ALLOC(1, slSceneDoc);
	sd->doc = new rapidjson::Document();

	bool retDoc, retSchema = false;

	char* scene_contents;
	if (loadTextFile(scene_contents, data_dir, scene_file))
		retDoc = parseFile(sd->doc, scene_contents, scene_file);

	char* schema_contents;
	if (loadTextFile(schema_contents, data_dir, schema_file))
		retSchema = parseFile(&schema_doc, schema_contents, schema_file);

	utilSafeFree(scene_contents);
	utilSafeFree(schema_contents);

	if (!retDoc || !retSchema || !validateJSON(&schema_doc, sd->doc, scene_file)) {
		slFreeSceneDoc(sd);
		return nullptr;
	}

	g_data_dir = data_dir;
	return sd;
}

void
slFreeSceneDoc(slSceneDoc* sd)
{
	if (sd != nullptr && sd->doc != nullptr) {
		delete sd->doc;
		sd->doc = nullptr;
	}

	utilSafeFree(sd);
}

bool
slParseEntities(slSceneDoc* sd,	Entity* entity_array, uint& entity_count, uint max_entities)
{
	const rapidjson::Value& entities = (*sd->doc)["entities"];
	if (entities.Size() > max_entities) {
		LOG(ERROR) << "entity count: " << entities.Size() << ", was more than max_entities: "
			<< max_entities << "\n";
		return false;
	}

	for (uint i = 0; i < entities.Size(); i++) {
		Entity& e = entity_array[i];
		e.scale = parseVec3(entities[i]["scale"]);
		e.use_grid = entities[i]["use_grid"].GetBool();

		if (e.use_grid) {
			const rapidjson::Value& grid_pos = entities[i]["grid_pos"];
			e.grid_pos.q = grid_pos["q"].GetInt();
			e.grid_pos.r = grid_pos["r"].GetInt();
			e.grid_pos.s = grid_pos["s"].GetInt();
		} else {
			e.translation = parseVec3(entities[i]["position"]);
		}

		uint max_len = 256;	// TODO: make define for string property max_len
		std::string model_file_str(entities[i]["model_file"].GetString());
		utilCopyCStr(e.model_filename,	model_file_str.c_str(), max_len);

		entity_count++;
	}

	return true;
}

void
slParseCamera(slSceneDoc* sd, camera& cam)
{
	const rapidjson::Value& json_cam = (*sd->doc)["camera"];
	cameraInitPerspective(cam,
		parseVec3(json_cam["position"]),
		parseVec3(json_cam["target"]),
		parseVec3(json_cam["world_up"])
	);
}

void
slParseHexGrid(slSceneDoc* sd, hexgrid& hg, util_image& palette_image)
{
	const rapidjson::Value& json_grid = (*sd->doc)["hex_grid"];

	std::string grid_type_str = json_grid["grid_type"].GetString();
	if (grid_type_str == "hexagon") {
		hg.gridT = HEXAGON;
		hg.hex_radius = json_grid["hex_radius"].GetInt();
	}
	else if (grid_type_str == "parallelogram")
		hg.gridT = PARALLELOGRAM;
	else if (grid_type_str == "rhombus")
		hg.gridT = RHOMBUS;
	else if (grid_type_str == "hash_map") {
		hg.gridT = HASH_MAP;
		// NOTE: 256 is the size of hg.map_file[256]
		// TODO: should probably define max_len as a constant somewhere
		utilCopyCStr(hg.map_file, utilBaseName(json_grid["map_file"].GetString()), 256);
	}

	glm::vec3 pos = parseVec3(json_grid["position"]);
	hg.position.x = pos.x;
	hg.position.y = pos.y;
	hg.position.z = pos.z;
	hg.hex_size = json_grid["hex_size"].GetInt();

	hg.fill_color_uv = utilGetPaletteCoords(palette_image, json_grid["fill_color"].GetInt());
	hg.selected_fill_color_uv = utilGetPaletteCoords(palette_image,
			json_grid["selected_fill_color"].GetInt());
	hg.line_color_uv = utilGetPaletteCoords(palette_image, json_grid["hex_line_color"].GetInt());

	std::string orientation_str = json_grid["hexlib_orientation"].GetString();
	hg.layout_mode = (orientation_str == "layout_flat") ? LAYOUT_FLAT : LAYOUT_POINTY;
}

bool
slParseGridHashMap(hexgrid& hg, const char* data_dir, const char* hexmap_schema_file)
{
	LOG(DEBUG) << "loading map_file: " << hg.map_file << "\n";

	char* contents;
	if (!loadTextFile(contents, data_dir, hg.map_file))
		return false;

	// NOTE: allow creating/saving over blank files
	if (contents[0] == 0 || contents[0] == '\n') {
		utilSafeFree(contents);
		LOG(INFO) << "empty map file\n";
		return true;
	}

	rapidjson::Document doc, schema_doc;
	bool retDoc, retSchema = false;
	char* schema_contents;

	retDoc = parseFile(&doc, contents, hg.map_file);

	if (loadTextFile(schema_contents, data_dir, hexmap_schema_file))
		retSchema = parseFile(&schema_doc, schema_contents, hexmap_schema_file);

	utilSafeFree(contents);
	utilSafeFree(schema_contents);

	if (retDoc && retSchema && validateJSON(&schema_doc, &doc, hg.map_file)) {
		hg.hex_size = doc["hex_size"].GetInt();
		std::string orientation_str = doc["layout_mode"].GetString();
		hg.layout_mode = (orientation_str == "layout_flat") ? LAYOUT_FLAT : LAYOUT_POINTY;
		glm::vec3 v = parseVec3(doc["world_position"]);
		hg.position.x = v.x; hg.position.y = v.y; hg.position.z = v.z;
		glm::vec3 n = parseVec3(doc["world_normal"]);
		hg.normal.x = n.x; hg.normal.y = n.y; hg.normal.z = n.z;
		rapidjson::Value& hexes = doc["hexes"];

		for (uint i = 0; i < doc["hexes"].Size(); i++) {
			// TODO: could re-use hexgrid.createHexInfo() here
			hex_info hxi;
			hxi.hexID = i;
			hxi.selected = false;
			hxi.XPos = hexes[i]["XPos"].GetFloat();
			hxi.YPos = hexes[i]["YPos"].GetFloat();
			hxi.hex.q = hexes[i]["q"].GetInt();
			hxi.hex.r = hexes[i]["r"].GetInt();
			hxi.hex.s = hexes[i]["s"].GetInt();

			for (uint j = 0; j < hexes[i]["vertices"].Size(); j++) {
				rapidjson::Value& v = hexes[i]["vertices"][j];
				glm::vec3 v3 = parseVec3(v);
				Point p(v3.x, v3.y);
				hxi.vertices.push_back(p);
			}

			hxi.vertices.shrink_to_fit();
			hg.hex_map.insert({hxi.hex, hxi});
		}

		return true;
	}

	LOG(ERROR) << "Error validation JSON\n";
	return false;
}

bool
slParseLights(slSceneDoc* sd, rg_point_light* lights, uint& num_lights, uint max_lights)
{
	const rapidjson::Value& json_lights = (*sd->doc)["lights"];

	if (json_lights.Size() > max_lights) {
		LOG(ERROR) << "Too many lights\n";
		return false;
	}

	num_lights = json_lights.Size();

	for (uint i = 0; i < num_lights; i++) {
		lights[i].position = parseVec3(json_lights[i]["position"]);
	}

	return true;
}

bool
slSaveGridFile(hexgrid& hg)
{
	uint max_len = 256;
	char full_path[max_len];
	if (utilConcatPath(full_path, g_data_dir, hg.map_file, max_len)) {
		LOG(INFO) << "saving grid file: " << full_path << "\n";

		rapidjson::StringBuffer sb;
		rapidjson::Writer<rapidjson::StringBuffer> writer(sb);

		writer.StartObject();

		writer.Key("hex_size"); writer.Uint(hg.hex_size);
		writer.Key("layout_mode");
		writer.String((hg.layout_mode == LAYOUT_FLAT) ? "layout_flat" : "layout_pointy");
		writer.Key("world_position");
		addV3f(writer, hg.position.x, hg.position.y, hg.position.z);
		writer.Key("world_normal");
		addV3f(writer, hg.normal.x, hg.normal.y, hg.normal.z);

		writer.Key("hexes");
		writer.StartArray();

		for (auto& it : hg.hex_map) {
			hex_info& hxi = it.second;
			writer.StartObject();
			writer.Key("XPos"); writer.Double(hxi.XPos);
			writer.Key("YPos"); writer.Double(hxi.YPos);
			writer.Key("q"); writer.Int(hxi.hex.q);
			writer.Key("r"); writer.Int(hxi.hex.r);
			writer.Key("s"); writer.Int(hxi.hex.s);
			writer.Key("vertices");
			writer.StartArray();

			for (uint i = 0; i < hxi.vertices.size(); i++) {
				// TODO: hard coded z value for hex vertices
				Point p = hxi.vertices[i];
				addV3f(writer, p.x, p.y, 0.f);
			}

			writer.EndArray();
			writer.EndObject();
		}

		writer.EndArray();

		writer.EndObject();

		if (writer.IsComplete()) {
			if (utilWriteTextFile(full_path, sb.GetString())) {
				LOG(INFO) << "save successful\n";
				return true;
			}

			LOG(ERROR) << "error saving file: " << full_path << "\n";
			return false;
		}

		LOG(ERROR) << "malformed json\n";
		return false;
	}

	LOG(ERROR) << "error creating path\n";
	return false;
}


// internal

bool
loadTextFile(char*& output, const char* data_dir, const char* file_name)
{
	bool ret = true;
	std::string file_path;
	file_path.append(data_dir).append("/").append(file_name);
	output = utilDumpTextFile(file_path.c_str());

	if (output == nullptr) {
		LOG(ERROR) << "Error reading file: " << file_name << "\n";
		ret = false;
	}

	return ret;
}

bool
parseFile(rapidjson::Document* doc, const char* file_contents, const char* file_name)
{
	rapidjson::ParseResult pr = doc->Parse(file_contents);

	if (!pr || !doc->IsObject()) {
		LOG(ERROR) << "Error parrsing json file: " << file_name;
		LOG(ERROR) << ". Error desscription: " << rapidjson::GetParseError_En(pr.Code())
			<< " (" << pr.Offset() << ")\n";
		return false;
	}

	return true;
}

bool
validateJSON(rapidjson::Document* schema_doc, rapidjson::Document* doc, const char* json_file)
{
	rapidjson::SchemaDocument schema(*schema_doc);
	rapidjson::SchemaValidator validator(schema);

	if (!doc->Accept(validator)) {
		LOG(ERROR) << "Error validating scene file: " << json_file << "\n";
		rapidjson::StringBuffer sb;
		validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
		LOG(ERROR) << "Invalid schema: " << sb.GetString() << "\n";
		LOG(ERROR) << "Invalid keyword:" << validator.GetInvalidSchemaKeyword() << "\n";
		sb.Clear();
		validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
		LOG(ERROR) << "Invalid Document: " << sb.GetString() << "\n";
		return false;
	}

	LOG(INFO) << "JSON file: " << json_file << " passed schema validation\n";

	return true;
}

glm::vec3
parseVec3(const rapidjson::Value& node)
{
	glm::vec3 v3;
	v3.x = node["x"].GetFloat();
	v3.y = node["y"].GetFloat();
	v3.z = node["z"].GetFloat();

	return v3;
}

glm::vec4
parseVec4(const rapidjson::Value& node)
{
	glm::vec4 v4;
	v4.x = node["x"].GetFloat();
	v4.y = node["y"].GetFloat();
	v4.z = node["z"].GetFloat();
	v4.w = node["w"].GetFloat();

	return v4;
}

void
addV3f(rapidjson::Writer<rapidjson::StringBuffer>& writer, float x, float y, float z)
{
	writer.StartObject();
	writer.Key("x");
	writer.Double(x);
	writer.Key("y");
	writer.Double(y);
	writer.Key("z");
	writer.Double(z);
	writer.EndObject();
}
