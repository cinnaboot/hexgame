
#include <cassert>

#if defined (_WIN32)
	#include <SDL.h>
#else
	#include <SDL2/SDL.h>
#endif

#include "imgui.h"
#include "examples/imgui_impl_sdl.h"
#include "examples/imgui_impl_opengl3.h"

#include "render/camera.h"
#include "render/entity.h"
#include "hexgrid.h"
#include "gooey.h"
#include "util.h"
#include "scene_loader.h"

#define GOOEY_BG_ALPHA 0.3f


// forward declarations
void renderMenuBar();
void renderRendererWindow(render_state* rs, ImGuiWindowFlags window_flags);
void renderHexgridWindow(hexgrid& grid, ImGuiWindowFlags window_flags);
void renderHexInfo(hex_info* hxi);
void renderGSWindow(game_state* gs, ImGuiWindowFlags window_flags);


bool
gooInit(SDL_Handles &handles, v2i vp_dims)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.IniFilename = NULL;	// don't save window state to imgui.ini

	ImGui_ImplSDL2_InitForOpenGL(handles.window, handles.glContext);
	ImGui_ImplOpenGL3_Init("#version 330 core");

	io.DisplaySize.x = vp_dims.x;
	io.DisplaySize.y = vp_dims.y;
	ImGui::StyleColorsDark();

	return true;
}

void
gooFree()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
}

bool
gooProcessEvent(SDL_Event &event)
{
	ImGui_ImplSDL2_ProcessEvent(&event);
	return ImGui::GetIO().WantCaptureMouse;
}

void
gooRender(game_state* gs, render_state* rs)
{
	assert(gs != nullptr && rs != nullptr);

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(rs->handles.window);
	ImGui::NewFrame();

	ImGuiWindowFlags window_flags = 0;
	window_flags |= ImGuiWindowFlags_NoScrollbar;
	window_flags |= ImGuiWindowFlags_NoMove;
	window_flags |= ImGuiWindowFlags_NoResize;
	window_flags |= ImGuiWindowFlags_NoCollapse;

	// v2i rs->viewport_dims;

	uint top_pad = 20;
	renderMenuBar();

	uint rw_width = 250;
	uint rw_height = 260;
	ImGui::SetNextWindowPos(ImVec2(rs->viewport_dims.x - rw_width, top_pad));
	ImGui::SetNextWindowSize(ImVec2(rw_width, rw_height));
	renderRendererWindow(rs, window_flags);

	uint ew_height = 250;
	ImGui::SetNextWindowPos(ImVec2(0, top_pad));
	ImGui::SetNextWindowSize(ImVec2(350, ew_height));
	renderGSWindow(gs, window_flags);

	uint hgw_height = 370;
	ImGui::SetNextWindowPos(ImVec2(0, top_pad + ew_height));
	ImGui::SetNextWindowSize(ImVec2(350, hgw_height));
	renderHexgridWindow(gs->grid, window_flags);

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}


// internal

void
renderMenuBar()
{
	ImGuiIO& io = ImGui::GetIO();

	ImGui::BeginMainMenuBar();
		if (ImGui::BeginMenu("Test")) {
			if (ImGui::MenuItem("Test 1")) {}
			ImGui::Separator();
			if (ImGui::BeginMenu("Add Widget")) {
				if (ImGui::MenuItem("Renderer")) {}
				if (ImGui::MenuItem("Hexgrid")) {}
				ImGui::EndMenu();
			}
			static bool br = true;
			static bool bh = true;
			ImGui::Checkbox("Show Renderer Widget", &br);
			ImGui::Checkbox("Show Hexgrid Widget", &bh);

			ImGui::EndMenu();
		}
		ImGui::SameLine(0, 800);
		ImGui::Text("%.3f ms/frame, (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
	ImGui::EndMainMenuBar();
}

void
renderRendererWindow(render_state* rs, ImGuiWindowFlags window_flags)
{
	ImGui::SetNextWindowBgAlpha(GOOEY_BG_ALPHA);
	ImGui::Begin("Renderer", nullptr, window_flags);

	ImGui::Text("viewport dims: %i, %i", rs->viewport_dims.x, rs->viewport_dims.y);
	ImGui::Checkbox("Debug Render", &rs->is_debug_draw);

	if (ImGui::CollapsingHeader("Camera Position", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Text("x: %f", rs->cam.position.x);
		ImGui::Text("y: %f", rs->cam.position.y);
		ImGui::Text("z: %f", rs->cam.position.z);
	}

	if (ImGui::CollapsingHeader("Camera Forward", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Text("x: %f", rs->cam.forward.x);
		ImGui::Text("y: %f", rs->cam.forward.y);
		ImGui::Text("z: %f", rs->cam.forward.z);
	}

	ImGui::Spacing();
	ImGui::Text("hAngle: %f", rs->cam.hAngle);
	ImGui::Text("vAngle: %f", rs->cam.vAngle);

	ImGui::End();
}

void
renderGSWindow(game_state* gs, ImGuiWindowFlags window_flags)
{
	ImGui::SetNextWindowBgAlpha(GOOEY_BG_ALPHA);
	ImGui::Begin("Game State", nullptr, window_flags);

	ImGui::Text("Select Mode");
	if (ImGui::RadioButton("Hex", (gs->select_mode == HEX_SELECT)))
		gs->select_mode = HEX_SELECT;
	if (ImGui::RadioButton("Entity", (gs->select_mode == ENTITY_SELECT)))
		gs->select_mode = ENTITY_SELECT;

	if (gs->selected_entity) {
		// TODO: need to implement entity selection
	}

	ImGui::End();
}

void
renderHexgridWindow(hexgrid& grid, ImGuiWindowFlags window_flags)
{
	ImGui::SetNextWindowBgAlpha(GOOEY_BG_ALPHA);
	ImGui::Begin("Hexgrid", nullptr, window_flags);

	// NOTE: only show file dialog for hash_map grid type
	if (grid.gridT == HASH_MAP) {
		ImGui::Text(grid.map_file);
		if (ImGui::Button("Save")) {
			slSaveGridFile(grid);
		}
	}

	ImGui::Text("Draw Mode:");
	if (ImGui::RadioButton("None", (grid.draw_mode == NONE)))
		grid.draw_mode = NONE;
	if (ImGui::RadioButton("Fill", (grid.draw_mode == FILL)))
		grid.draw_mode = FILL;
	if (ImGui::RadioButton("Line", (grid.draw_mode == LINE)))
		grid.draw_mode = LINE;
	if (ImGui::RadioButton("Cone Fill", (grid.draw_mode == CONE_FILL)))
		grid.draw_mode = CONE_FILL;
	if (ImGui::RadioButton("Add Hexes", (grid.draw_mode == ADD_HEXES)))
		grid.draw_mode = ADD_HEXES;
	if (ImGui::RadioButton("Remove Hexes", (grid.draw_mode == REMOVE_HEXES)))
		grid.draw_mode = REMOVE_HEXES;

	ImGui::Separator();
	ImGui::Text("is_selecting");
	ImGui::SameLine(); ImGui::TextUnformatted(grid.is_selecting ? "true" : "false");
	ImGui::Text("Hex Count: %lu", grid.hex_map.size());

	ImGui::Separator();
	ImGui::Text("current_hex: ");
	if (grid.current_hex)
		renderHexInfo(grid.current_hex);

	ImGui::Separator();
	ImGui::Text("start_hex: ");
	if (grid.start_hex)
		renderHexInfo(grid.start_hex);

	ImGui::Separator();

	ImGui::End();
}

void
renderHexInfo(hex_info* hxi)
{
	Hex h = hxi->hex;
	ImGui::SameLine(); ImGui::Text("%i, %i, %i", h.q, h.r, h.s);
	ImGui::SameLine(); ImGui::Text(", selected: %s", (hxi->selected) ? "true" : "false");
	ImGui::Text("hexID: %u", hxi->hexID);
	ImGui::Text("world position x,y: %f,%f", hxi->XPos, hxi->YPos);
}
