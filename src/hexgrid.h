
#pragma once

#include <vector>
#include <unordered_map>

#include "hexlib.h"
#include "render/render_group.h"
#include "util.h"


// TODO: rename to "hex_draw_mode" to fit project style
enum HexDrawMode
{
	NONE,
	FILL,
	LINE,
	CONE_FILL,
	PATHFINDING,
	ADD_HEXES,
	REMOVE_HEXES
};

// TODO: implement more hexgrid types
enum grid_type
{
	HEXAGON,
	PARALLELOGRAM,
	RHOMBUS,
	HASH_MAP
};

enum hex_layout_mode
{
	LAYOUT_FLAT,
	LAYOUT_POINTY
};

struct hex_info
{
	uint hexID;
	Hex hex = {};
	real64 XPos;
	real64 YPos;
	bool selected;
	std::vector<Point> vertices;

	void* contained_entity;
};

struct hex_hashfunc
{
	// NOTE: hash_combine from boost
	size_t operator()(const Hex& h) const {
		std::hash<int> int_hash;
		size_t hq = int_hash(h.q);
		size_t hr = int_hash(h.r);
		return hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2));
	}
};

struct hexgrid
{
	grid_type gridT;
	hex_layout_mode layout_mode;
	Layout hexlib_layout;
	v3f position;
	v3f normal;
	uint hex_size;

	// NOTE: all these properties aren't used by every map type
	char map_file[256];
	uint hex_radius;

	v2f fill_color_uv;
	v2f selected_fill_color_uv;
	v2f line_color_uv;

	std::unordered_map<Hex, hex_info, hex_hashfunc> hex_map;

	hex_info* start_hex;
	hex_info* current_hex;
	HexDrawMode draw_mode;
	bool is_selecting;
};


bool hgInit(
		hexgrid& hg,
		render_group*& rg_filled,
		render_group*& rg_lines,
		rg_shader_program& shader);

bool hgAddHex(hexgrid& hg, v3f hex_coords, render_group* rg_filled, render_group* rg_lines);

bool hgRemoveHex(hexgrid& hg, v3f hex_coords, render_group* rg_filled, render_group* rg_lines);

void hgUpdateUVBuffer(hexgrid& hg, render_group* rg);

hex_info* hgGetSingleHex(hexgrid& hg, real32 x, real32 y);

hex_info* hgGetSingleHex(hexgrid& hg, int q, int r, int s);

v3f hgGetWorldPostion(hexgrid& hg, int q, int r, int s);

void hgResetHexes(hexgrid& hg);

void hgUpdateHexFill(hexgrid& hg, int32 x, int32 y);

void hgUpdateHexLineDraw(hexgrid& hg, int32 x, int32 y);

void hgUpdateHexConeFill(hexgrid& hg, int32 x, int32 y, float cone_angle, std::vector<Point>& debug_vertices);

