
#pragma once

#include "hexgame.h"
#include "render/renderer.h"

bool gooInit(SDL_Handles &handles, v2i vp_dims);

void gooFree();

bool gooProcessEvent(SDL_Event &event);

void gooRender(game_state* gs, render_state* rs);

