
## TODO:
- position entities on grid hexes
- pathfinding
- assimp animation
- map generation
- gooey
	- add object inspector window
		- modify position, scaling, rotation
		- show vertex count, mesh filename, ...
	- add 3d orientation widget to gooey
- renderer
	- add name property to render_group to help debugging
	- consolodate textures into just the render_state->palette_image
		- update copyMeshInfo() and meMeshInfo struct to remove image loading from filename
		- update rgInitEntity() to set tex_id of palette_image
		- remove render_object->use_texture
	- check over renderer, camera, gooey init after changes
	- pass in frame time to camera movement functions to decouple speed from framerate
	- test camera speed when moving with composite vector
		- may be fixed?? need to test by examining camera position between frames and compare the distance
	- debug lighting (using multiple lights doesn't add correctly atm
		- move glUniform call to lights[i].position out of rgDraw, and into scene_loader init phase
		- show debug mesh for light positions
		- attenuate point lights in shader
	- simplify render_object buffers into 2d array or vector, and use stride when pushing to GL
	- render debug bounding boxes for meshes (including hexgrid)
- remove checks for colors, textures from render_group functions
	- actually cannot remove checks for texture because some meshes aren't loaded properly
	- maybe can cause loading mesh to fail when more than one mesh per file
- figure out bug in fragment shader when removing commented line
- move input handling out to new file, controller?
- replace aixlog with custom logging iostream?
- think about combining mesh data and render_group data to save some memory
	- actually don't need to save the vertex/normal buffers after passing to opengl
- use a storage pool for assimp meshes allowing reuse across entities
- remove v2i/v3f... etc from util.h and either use glm everywhere, or write a smaller linear math.h
- add an application config file and structure to replace various defines
	- be sure to put a max_property_length or similar for c-string properties

## LATER TODO:
- add initial opengl constant for INT_MAX, and checks in render_group functions
- try out YAML instead of JSON (make sure can still use schema validation)
	- there's a bug when changing hex_grid.hex_radius from a 2 digit number to a 1 digit number in test_scene.json
	- also noticing more bugs with rapidjson schema validation, "required" fields don't seem to always cause validation to fail
- move from COLLADA output to glTF (GL Transmission Format) https://www.khronos.org/gltf/
- add config.js/yaml for application config (remove some defines everywhere)
- maybe try deferred rendering at some point to use a ton of lights
- use assimp material info in shaders for fancier lighting
- add fancy GL error handler https://gist.github.com/liam-middlebrook/c52b069e4be2d87a6d2f
- add cpu performance counters in render loop
	- hgUpdateUVBuffer can probably be improved by adding selected hexes to a small cache structure
	- can also try passing in small chunks with glBufferSubData instead of overwriting whole buffer
- check for memory leaks w/ valgrind

## DONE:
