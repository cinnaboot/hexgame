

CXX = g++
CXXFLAGS = -std=c++11 -g -ggdb3 -Wall -I/usr/include/SDL2 -Iext/aixlog/include -Iext/stb_libs
SRCDIR = src
OBJDIR = build

# TODO: incorporate imgui into project files and not use gl3w
# 	can also remove -ldl from LDFLAGS then
CC = gcc
GL3W_CFLAGS = -g -Wall -I$(IMGUI_DIR)/examples/libs/gl3w
CXXFLAGS += -I$(IMGUI_DIR)/examples/libs/gl3w
GL3W_LDFLAGS += -ldl

# libGLEW needs to be >= 2.0.0 to use opengl core context
#	don't know a good way to require this in make without forcing a specific version
#LDFLAGS = -lSDL2 -lGLEW -lGL
LDFLAGS = -lSDL2 -lGL

# IMGUI
IMGUI_DIR = ext/imgui
CXXFLAGS += -I$(IMGUI_DIR) \
			-I$(IMGUI_DIR)/examples/ \
			-I$(IMGUI_DIR)/examples/libs/gl3w
IMGUI_SOURCES = $(IMGUI_DIR)/imgui.cpp \
		  $(IMGUI_DIR)/imgui_demo.cpp \
		  $(IMGUI_DIR)/imgui_draw.cpp \
		  $(IMGUI_DIR)/imgui_widgets.cpp \
		  $(IMGUI_DIR)/examples/imgui_impl_opengl3.cpp \
		  $(IMGUI_DIR)/examplex/imgui_impl_sdl.cpp \

IMGUI_OBJECTS := $(patsubst %.cpp, $(OBJDIR)/%.o, $(notdir $(IMGUI_SOURCES)))

# assimp
LDFLAGS += -lassimp


SOURCES = $(wildcard $(SRCDIR)/*.cpp)
OBJECTS = $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SOURCES))


all: $(IMGUI_OBJECTS) $(OBJDIR)/gl3w.o $(OBJECTS) renderer
	$(CXX) -o $(OBJDIR)/hexgame $(LDFLAGS) $(GL3W_LDFLAGS) $(OBJDIR)/*.o $(OBJDIR)/librender.a
	mkdir -p bin
	mv $(OBJDIR)/hexgame bin
.PHONY: all

renderer:
	$(MAKE) -C $(SRCDIR)/render
	cp $(SRCDIR)/render/build/librender.a $(OBJDIR)

-include $(OBJDIR)/*.d

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

info:
	@echo "SOURCES:"
	@echo $(SOURCES)
	@echo "OBJECTS:"
	@echo $(OBJECTS)

# NOTE: couldn't find a nicer way to put these object files into build directory :(
$(OBJDIR)/imgui.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/imgui.cpp -o $@

$(OBJDIR)/imgui_demo.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/imgui_demo.cpp -o $@

$(OBJDIR)/imgui_draw.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/imgui_draw.cpp -o $@

$(OBJDIR)/imgui_widgets.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/imgui_widgets.cpp -o $@

$(OBJDIR)/imgui_impl_opengl3.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/examples/imgui_impl_opengl3.cpp -o $@

$(OBJDIR)/imgui_impl_sdl.o:
	$(CXX) -c $(CXXFLAGS) $(IMGUI_DIR)/examples/imgui_impl_sdl.cpp -o $@

$(OBJDIR)/gl3w.o:
	$(CC) -c $(GL3W_CFLAGS) $(IMGUI_DIR)/examples/libs/gl3w/GL/gl3w.c -o $@

.PHONY: clean
clean:
	rm -f bin/hexgame $(OBJDIR)/*
	$(MAKE) -C $(SRCDIR)/render clean
